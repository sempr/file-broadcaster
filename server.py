#encoding=utf-8
#simple http/sockJS comet server written by huangdx
import random
import urllib
import asyncmongo
import os
import time
from bson.objectid import ObjectId

import tornado.ioloop
import tornado.web
import tornado.gen
import sockjs.tornado
import json
import base64

from tornado.options import define, options

define("port", default=18081, help="run on the given port", type=int)

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", IndexHandler),
            (r"/new", CreateHandler),
            (r"/test", TestHandler),
            (r"/test2", Test2Handler),
            (r"/list", ListHandler),
            (r'/(\d{8})/upload',UploadHandler),
            (r'/(\d{8})/list',ListContentHandler),
            (r'/(\d{8})/browser',ListBrowserHandler),
            (r'/(\d{8})/',WaitPageHandler),
            (r'/(\d{8})/download/([0-9a-f]{24})/',DownloadHandler),
        ] + CometService.urls
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=False,
            cookie_secret="11oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdxP1o/Vo=",
            autoescape=None,
            debug=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        self.db = asyncmongo.Client(pool_id='wdj_pool', host='127.0.0.1',
            port=27017, dbname='wdj_test',maxcached=10, maxconnections=50)


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

    @tornado.web.asynchronous
    #@tornado.gen.engine
    def test(self):
        #tornado.gen.Task(self.db.files.find,{'channel_id':1},fields=['user_id','session_m'])
        pass

class IndexHandler(BaseHandler):
    """ The Index Page"""
    def get(self):
        self.render('upload.html')


class WaitPageHandler(BaseHandler):
    """ The Wait Page For the Channel"""
    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self, channel):
        last_hour = time.time()-3600
        r,_ = yield tornado.gen.Task(self.db.files.find_one,{'channel':int(channel),'ts':{'$gt':last_hour}},sort=[('ts',asyncmongo.DESCENDING)])
        if r[0]:
            last_file = r[0]
            last_file['ts']=int(time.time()-last_file['ts'])
            last_file['url']='%d/download/%s/'%(last_file['channel'],str(last_file['_id']))
        else:
            last_file = None
        self.render('wait.html',last_file=last_file)


class ListBrowserHandler(BaseHandler):
    def get(self, channel):
        self.render('list.html', tttt=channel)

class ListContentHandler(BaseHandler):
    """ List all files of this user"""
    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self,channel):
        ts = time.time()
        r,_ = yield tornado.gen.Task(self.db.files.find,{'channel':int(channel)},sort=[('ts',asyncmongo.DESCENDING)],limit=10)
        ret = {'res': []}
        for a in r[0]:
            ret['res'].append({
                'filename': a['filename'],
                'file_size': a['file_size'],
                'channel': a['channel'],
                '_id': str(a['_id']),
                'ts':int(ts-a['ts']),
                'download_times':a.get('download_times',0),
                'url':'/%d/download/%s/'%(a['channel'],str(a['_id'])),
            })
        self.write(json.dumps(ret))
        self.finish()

class DownloadHandler(BaseHandler):
    """ get file by _id """
    @tornado.web.asynchronous
    @tornado.gen.engine
    def get(self, channel, fid):
        r,_ = yield tornado.gen.Task(self.db.files.find,{'_id':ObjectId(fid),'channel':int(channel)})
        if len(r[0])>0:
            file_url = r[0][0]['file_url']
            the_file = r[0][0]
            suffix = the_file['filename'][-3:].lower()
            if suffix not in set(['png','jpg','gif']):
                self.set_header("Content-Type", the_file['content_type'])
                self.set_header('Content-Disposition','attachment; filename=%s'%the_file['filename'])
            else:
                self.set_header("Content-Type", 'image/%s'%suffix)
            self.set_header('Content-Length', the_file['file_size'])
            f = open(the_file['file_path'],'rb')
            self.write(f.read())
            f.close()
            r,_ = yield tornado.gen.Task(self.db.files.update,{'_id':ObjectId(fid)},{'$inc':{'download_times':1}})
            self.finish()
        else:
            self.write('404 not found')
            self.set_status(404)
            self.finish()

class Test2Handler(BaseHandler):
    def get(self):
        self.render('test2.html')
class TestHandler(BaseHandler):
    def post(self):
        binary = base64.b64decode(self.get_argument('data'))
        filename = self.get_argument('filename')
        #file = {'body':binary,'filename':filename,'content_type':'application/file'}
            
        file_path = os.path.join(os.path.dirname(__file__),'test.txt')
        f = open(file_path, 'wb')
#        print binary, len(binary),
        f.write(binary)
        f.close()
#        print self.get_argument('data')
#        print self.get_argument('filename')
        pass
    def get(self):
        self.render('test3.html')
    

class UploadHandler(BaseHandler):
    """ Handle Upload, and QR Code """
    def get(self, channel):
        self.render('upload.html',channel=channel)

    @tornado.web.asynchronous
    @tornado.gen.engine
    def post(self,channel):
        p_type = self.get_argument('type','nor')
        if p_type == 'b64':
            binary = base64.b64decode(self.get_argument('data'))
            filename = self.get_argument('filename')
            file = {'body':binary,'filename':filename,'content_type':'application/file'}
        else:
            file = self.request.files['file'][0]
        #print file
        # save file first
        file_path = os.path.join(os.path.dirname(__file__),"static","upload",str(int(time.time())))
        os.makedirs(file_path)
        file_name = os.path.join(file_path,file['filename'])
        f = open(file_name,'wb')
        f.write(file['body'])
        f.close()
        file_url = os.path.join("static","upload",str(int(time.time())),file['filename'])

        doc = {
            'file_path':file_name,
            'file_url': urllib.quote(file_url.encode('utf-8'),safe='/'),
            'filename':file.get('filename'),
            'content_type':file.get('content_type'),
            'file_size':len(file.get('body')),
            'channel': int(channel),
            'ts': time.time(),
            'download_times':0
        }
        r,_ = yield tornado.gen.Task(self.db.files.insert,doc)
        
        r,_ = yield tornado.gen.Task(self.db.files.find_one,{'channel':int(channel)},sort=[('ts',asyncmongo.DESCENDING)])
        doc = r[0]
        #self.write('ok!'+channel+str(doc))
        doc['_id'] = str(doc['_id'])
        push_message(str(doc['channel']), json.dumps(doc))
        self.finish()

class CreateHandler(BaseHandler):
    """ Create A new Channel """
    def get(self):
        channel = random.randint(10000000,99999999)
        self.redirect('/%d/upload'%channel)

class ListHandler(BaseHandler):
    """ Create A new Channel """
    def get(self):
        self.render('list.html', tttt=0)


class CometConnection(sockjs.tornado.SockJSConnection):
    channel_map = dict()

    def on_open(self, info):
        print 'here'
        pass

    def on_message(self, message):
        data = json.loads(message)
        channel = data['channel_name']
        cmd = data['cmd']
        self.channel = channel
        if cmd == 'sub':
            waiters = self.channel_map.get(channel, [])
            waiters.append(self)
            self.channel_map[channel] = waiters

    def on_close(self):
        for channel_name , clients in self.channel_map.items():
            print channel_name, clients
            if clients is not None and self in clients:
                clients.remove(self)
                # useless?
                self.channel_map[channel_name] = clients

CometService = sockjs.tornado.SockJSRouter(CometConnection, '/comet')

# 上传文件
# 下载列表
# 文件下载
# 客户端推送
def push_message(channel_name, data):
    channel_map = CometConnection.channel_map
    channel = channel_map.get(channel_name, [])
    CometService.broadcast(channel, data)

if __name__ == '__main__':
    tornado.options.parse_command_line()
    import logging
    logging.getLogger().setLevel(logging.DEBUG)
    app = Application()
    
    app.listen(options.port)

    tornado.ioloop.IOLoop.instance().start()
