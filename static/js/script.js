var filename = "";
$(document).ready(function() {
	var t = randomFromInterval(10000000, 99999999);
	if(!localStorage['current_tunnel']) {
		localStorage['current_tunnel'] = t;
	}
	else if(localStorage['current_tunnel'] == 0) {
		localStorage['current_tunnel'] = t;
	}
	if($("#uploadPage").length > 0) {	
        var qrlink = 'http://'+window.location.host+'/'+localStorage['current_tunnel']+'/';
        var img_url = "http://chart.apis.google.com/chart?cht=qr&chl=" + encodeURIComponent(qrlink) + "&chs=400x400";
	    $(".qrcode").attr("src", img_url);
	    $(".qrlink").text(qrlink);
	    
	    $("#uploadBtn").click(function() {
			$("#fileToUpload").trigger("click");
		});
		var ttt = 0;
		function handleFileSelect(evt) {
			var file = evt.target.files[0]; // FileList object
			if(+file.size > 5000000) {
				$("#uploadBtn").after("<p class='small' id='aaa'>测试期间只支持5M以下的文件</p>");
				setTimeout(function() {
					$("#aaa").fadeOut(function() {
						$(this).remove();
					});
				}, 5000);
				
				return false;
			}
			$("#aaa").remove();
			$("#uploadBtn").after("<p class='small' id='aaa'>数据处理中...</p>");
			console.log(file.size + ' ' + file.name)
			var reader = new FileReader();
			var blob = file.webkitSlice(0)
			reader.readAsBinaryString(blob);
			reader.onload = function(e){
	            //console.log(this.result) 
	            var str = $.base64.encode(this.result);

    			$("#aaa").remove();
				$(".uploadStep").hide();
				$("#whenUpload").show();
				ttt = 0;
				$("#whenUpload #progressBar .now").css("width", "0%");
				var interval = setInterval(function() {
					ttt += 2;
					var percentComplete = Math.round(ttt * 100 / 100);
					if(ttt === 100) clearInterval(interval);
		        	$("#whenUpload #progressBar .now").css("width", percentComplete + "%");
				}, 50);
	            $.post('/'+localStorage['current_tunnel']+'/upload',{'type':'b64','filename':file.name,'data':str},function(){
	    			$(".uploadStep").hide();
	    			$("#afterUpload p.small").text(filename);
	    			$("#afterUpload").show();
	            });
	        }
		}
	  document.getElementById('fileToUpload').addEventListener('change', handleFileSelect, false);
	}
	/*
	var fileupload = $('#fileToUpload').fileupload({
        dataType: 'json',
        bitrateInterval: '50',
        url: '/'+localStorage['current_tunnel']+'/upload',
        start: function(e) {
			$(".uploadStep").hide();
			$("#whenUpload").show();
        },
        complete: function(e, data) {
			$(".uploadStep").hide();
			$("#afterUpload p.small").text(filename);
			$("#afterUpload").show();
        },
        progress: function(e ,data) {
        	var percentComplete = Math.round(data.loaded * 100 / data.total);
        	$("#whenUpload #progressBar .now").css("width", percentComplete + "%");
        },
        dropZone: $("#uploadArea")
    });*/
	
	if($("#waitPage").length > 0) {
		$("#recentlyList").attr("href", window.location.href+'browser');
	}
  
	$("#uploadAnotherBtn").click(function() {
		$(".uploadStep").hide();
		$("#beforeUpload").show();
	});
	$('#fileToUpload').change(function() {
		filename = $(this).val();
	});
	$(".howto").click(function() {
		loadModal("#qrmodal");
	});
	$("#modalBg").click(function() {
		closeModal();
	});
	
	
	/** list page **/
	if($("#listPage").length > 0) {
		var a = localStorage['current_tunnel'];
		if(tttt) {
			a = tttt;
		}
		$.get('/' + a + '/list', function(res) {
			res = res.res;
			var table = $("#listArea table tbody");
			for(var i=0; i<res.length; i++) {
                it = res[i];
				var tr = $("<tr></tr>");
				tr.append("<td>" + it['filename'] + "</td>");
				if(tttt == 0) {
					tr.append("<td>" + it['ts'] + "秒前</td>");
				}
				var size = +it['file_size']/1000;
				if(size > 1000) {
					size = Math.round(size/1000, 1) + 'MB';
				}
				else if(size > 1) {
					size = Math.round(size, 1) + 'KB';
				}
				else {
					size = size*1000 + 'B';
				}
				tr.append("<td>" + size + "</td>");
				if(tttt) {
					tr.append("<td style='text-align:right;padding-right:15px'><a target=_blank href=" + it['url'] +" class='blue'>下载</a></td>"); /* 这个id要重新拼一下 */
				}
				table.append(tr);
			}
		}, 'json');
	}	
});

loadModal = function(itemId) {
    $('#modalBg').show();
    $('#modalContainer').show();
    $(itemId).show();
    positionModal();
    $(".closeModal").click(function() {
        closeModal();
    });
};
closeModal = function() {
    $('.modal').hide();
    $('#modalContainer').hide();
    $('#modalBg').hide();
};
positionModal = function() {
    var wWidth  = window.innerWidth;
    var wHeight = window.innerHeight;

    if (wWidth==undefined) {
        wWidth  = document.documentElement.clientWidth;
        wHeight = document.documentElement.clientHeight;
    }

    var boxLeft = parseInt((wWidth / 2) - ( $("#modalContainer").width() / 2 ));
    var boxTop  = parseInt((wHeight / 2) - ( $("#modalContainer").height() / 2 ));

    // position modal
    $("#modalContainer").css({
        'margin': boxTop + 'px auto 0 ' + boxLeft + 'px'
    });

    $("#modalBg").css("opacity", 0.6);
    if($("body").height() > $("#modalBg").height()) {
        $("#modalBg").css("height", $("body").height() + "px");
    }
};
function domNodeIsContainedBy(node, containedByNode) {
    if (containedByNode.compareDocumentPosition)
        return (containedByNode.compareDocumentPosition(node) & 16) == 16;
	if(containedByNode == document && node.parentNode) {
		return true;
	}
    while (node != null) {
        if (node == containedByNode)
            return true;
        node = node.parentNode;
    }
    return false;
}
function randomFromInterval(from, to)
{
    return Math.floor(Math.random()*(to-from+1)+from);
}
